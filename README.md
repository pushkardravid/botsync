# README #

Clone Wars 2.0

### What is this repository for? ###

* Python project to sync multiple remote directories.
* Version 1.0.0
### How do I get set up? ###

* Preferred platform, Ubuntu. If trying on any Windows, need to write a bash to run the python files instead of tmux sessions.
* Clone the project to your local directory.
* Go to project and enter ```pip install -r requirements.txt``` to download all project dependencies.
* Go to https://www.dropbox.com/developers and create a developer app. Geneate access token for the same. Inside the app, create a folder that will be synced across all bots.
* Set the following environment variables:
	ACCESS_TOKEN,BOT_PATH,BOT_DIR  using the following command  
	```	
	export <variable_name>=<variable_value>
	```
* Open a tmux session by typing:  
	 ```tmux  ```  
	and enter the following command: ```python syncUp.py```  
	then press Ctrl + Shift + B. Then open a new tmux session and enter ```python syncDown.py``` . Close the session with Ctrl + Shift + B.  

	
	

### Who do I talk to? ###

* Author : Pushkar Dravid
* Contact : pushkarsdravid@gmail.com
