import os,time
import dropbox

ACCESS_TOKEN = os.environ['ACCESS_TOKEN'] #To be obtained from the dropbox app that you'd be creating
BOT_PATH = os.environ['BOT_PATH'] #The absolute path to the bot folder which is to be synced
BOT_DIR = os.environ['BOT_DIR'] #Name of the bot folder to be synced

dbx = dropbox.Dropbox(ACCESS_TOKEN) #Open a connection to dropbox app

#Keep fetching all files from dropbox app, every 5 seconds and update/write to the bot folder
while True:
    time.sleep(5)
    for entry in dbx.files_list_folder(BOT_DIR,recursive=True).entries:
        if isinstance(entry, dropbox.files.FileMetadata):
                file_path = entry.path_display
                base_folder = '/'.join(file_path.split('/')[:-1])
                file_path_loc = BOT_PATH + base_folder
                if not os.path.exists(file_path_loc):
                    try:
                        os.makedirs(file_path_loc)
                    except:
                        print "failed to create folder"
                dbx.files_download_to_file(file_path_loc+'/'+entry.name,file_path)
                print "downloaded " + file_path
				
				
				