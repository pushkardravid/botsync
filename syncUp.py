import os,time
import dropbox
from dropbox.files import WriteMode

ACCESS_TOKEN = os.environ['ACCESS_TOKEN'] #To be obtained from the dropbox app that you'd be creating
BOT_PATH = os.environ['BOT_PATH'] #The absolute path to the bot folder which is to be synced
BOT_DIR = os.environ['BOT_DIR'] #Name of the bot folder to be synced
BOT_PATH = BOT_PATH + BOT_DIR

dbx = dropbox.Dropbox(ACCESS_TOKEN)

def upload_file(path):
    f = open(path, 'rb')
    remote_path = BOT_DIR + path.split(BOT_DIR)[1]
    print remote_path
    dbx.files_upload(f.read(), remote_path, mute=True, mode=WriteMode('overwrite', None), )

t = 1

# Every 5 seconds, check all files inside the bot folder, and if the file has been modified/newly created, it is pushed to the dropbox app, which will be pulled by syncDown.py
while True:
    time.sleep(5)
    for subdir, dirs, files in os.walk(BOT_PATH):
        for file in files:
            file_obj = os.path.join(subdir, file)
            if os.path.getmtime(file_obj) > t:
                upload_file(file_obj)
                print "uploaded " + str(file_obj)
    t = time.time()